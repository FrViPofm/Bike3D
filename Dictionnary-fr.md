

## C

**Crank**: *Manivelle*

**Crankset**: *Pédalier*

## S

**Spindle** *Axe* de pédalier

* **square-tapered spindle** *Axe à emmanchement carré*
    * **nut-type spindle** *Axe pour écrou* ( filletage externe )
    * **bolt-type spindle** *Axe pour vis* (filetage interne )
* **splined spindle** *Axe cannelé*
